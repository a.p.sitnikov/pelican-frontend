SHELL=/bin/bash
.DEFAULT_GOAL:=help


.PHONY: help info
##@ Commands

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  \
	make increment-version build-and-deploy\n  \
	make [VARS] \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ \
	{ printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ \
	{ printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

################################################################################
PROJECT_VERSION := $(shell cat src/app/version/application.version.json | grep version | tr -d 'version: ' | tr -d '"')

info:
	echo ${PROJECT_VERSION}

serve: ## Debug run application
	cd builder && docker-compose up

increment-version: ## Increment application version
	cd builder && make increment-version
	$(eval PROJECT_VERSION := $(shell cat src/app/version/application.version.json | grep version | tr -d 'version: ' | tr -d '"' ))

update-version-variable:
	$(eval PROJECT_VERSION := $(shell cat src/app/version/application.version.json | grep version | tr -d 'version: ' | tr -d '"' ))

build-prod: ## Build proj for prod version
	cd builder &&  make npm-build-prod

publish:  ## Build and publish
	cd docker && make build push

build-project: build-prod publish ## Build project

test-prod: build-prod ## Run in test prod mode
	cd docker && docker-compose kill \
    && docker-compose rm -f && \
    && make docker-remove-image \
    make build \
    && docker-compose up

push: ## npm install
	cd docker && make publish

build-and-deploy: ## deployment on usque.ru
	cd docker && make build-and-deploy
