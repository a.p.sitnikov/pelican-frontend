#!/usr/bin/env bash

VER=latest

image=pelican-fronend-compile

image="${image}/sitnikov.a/angular-builder:${VER}"
docker build -f Dockerfile \
    --build-arg ANGULAR_CLI_VERSION=$VER \
    -t $image .
