import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';

@Injectable()
export class AppConfig {
    constructor() {
    }

    load() {
        for (const field in environment) {
            if (document['settings'] && document['settings'][field]) {
                environment[field] = document['settings'][field];
            }
        }
    }
}
