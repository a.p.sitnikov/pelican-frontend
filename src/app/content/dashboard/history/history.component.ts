import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProgressDay} from '../../shared/models/progress-day';
import {Category} from '../../shared/models/category';
import * as moment from 'moment';
import {ObjValue, TextValue} from '../../shared/models/text-value';
import PelicanUtils from '../../shared/pelicanUtils';

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

    @Input() progresses: ProgressDay[] = [];
    @Output() dateSelect = new EventEmitter<ProgressDay>();
    @Input() selectedProgressDay: ProgressDay;
    format = 'DD.MM.YYYY';
    nowDate = moment();
    nowString = this.nowDate.format(this.format);
    allCategories: Category[] = [];
    parentCategories: Category[] = [];
    selectedCategory: Category;
    CATEGORY_COLORS_MAPPING = 'CATEGORY_COLORS_MAPPING';
    mapChildrenToParentIdCategories: Array<[number, number]>;

    @Input()
    set categories(cats: Category[]) {
        this.allCategories = cats;
        this.parentCategories = this.allCategories.filter(e => e.parent === null);
        this.selectedCategory = this.parentCategories[0];
        this.restoreToStoredEventColors();
        this.mapChildrenToParentIdCategories = this.allCategories
            .filter(e => e.parent !== null)
            .map(x => [x.id, x.parent.id] as [number, number]);
    }

    selectedColor = '';
    storedEventColors: ObjValue<Category, string>[] = [];
    colors: string[] = [];
    isShownDisplayCard = false;

    constructor() {
        this.selectedProgressDay = this.progresses[0];
        this.dateSelect.emit(this.progresses[0]);

        this.colors.push('red');
        this.colors.push('orangered');
        this.colors.push('salmon');
        this.colors.push('tomato');
        this.colors.push('yellow');
        this.colors.push('gold');
        this.colors.push('orange');
        this.colors.push('chocolate');
        this.colors.push('plum');
        this.colors.push('deeppink');
        this.colors.push('fuchsia');
        this.colors.push('green');
        this.colors.push('aquamarine');
        this.colors.push('mediumspringgreen');
        this.colors.push('limegreen');
        this.colors.push('lime');
        this.colors.push('darkturquoise');
        this.colors.push('purple');
        this.colors.push('blue');
        this.colors.push('deepskyblue');
        this.colors.push('dodgerblue');
        this.colors.push('royalblue');
        this.colors.push('black');
        this.selectedColor = this.colors[0];
    }

    ngOnInit() {
    }


    selectDate(day: ProgressDay) {
        this.selectedProgressDay = day;
        this.dateSelect.emit(day);
    }

    dateOnlyDayNumber(date: ProgressDay): string {
        const day = moment(date.date, 'DD.MM.YYYY');
        // console.log(date);
        return day.format('D');
    }

    daysToSkip(dates: ProgressDay[]): string[] {
        const day = moment(dates[0].date, 'DD.MM.YYYY');
        // console.log(dates[0].date, day.isoWeekday());
        const days = day.isoWeekday() - 1;
        if (days === 0) {
            return [];
        }
        return Array(days).fill(days);
    }

    onParentSelect(event) {
        console.log('onParentSelect', event.target.value);
        if (event.target.value) {
            this.selectedCategory = this.parentCategories[event.target.value];
            console.log('onParentSelect', this.selectedCategory);
        }
    }

    removeEventColor(eventColor: ObjValue<Category, string>) {
        // console.log(eventColor);
        // this.storedEventColors.reduce()
        const index = this.storedEventColors.indexOf(eventColor, 0);
        if (index > -1) {
            this.storedEventColors.splice(index, 1);
            this.saveToLocalStorage();
        }
    }

    addSelectedColor() {
        const idxCtg = this.storedEventColors.findIndex(c => c.object.id === this.selectedCategory.id || c.value === this.selectedColor);
        if (idxCtg > -1) {
            return;
        }

        this.storedEventColors.push(new ObjValue<Category, string>(this.selectedCategory, this.selectedColor));
        this.saveToLocalStorage();
    }

    saveToLocalStorage() {
        const toStore = this.storedEventColors.map(e => new TextValue<string>(e.object.id.toString(), e.value));
        localStorage.setItem(this.CATEGORY_COLORS_MAPPING, PelicanUtils.object_stringify(toStore));
    }

    restoreToStoredEventColors() {
        const textValues: TextValue<string>[] = JSON.parse(localStorage.getItem(this.CATEGORY_COLORS_MAPPING));
        if (textValues) {
            this.storedEventColors = [];
            textValues.forEach(i => {
                const idx = this.parentCategories.findIndex(c => c.id === +i.text);
                if (idx > -1) {
                    this.storedEventColors.push(new ObjValue<Category, string>(this.parentCategories[idx], i.value));
                }
            });
        }
    }

    onSelectedColor(event) {
        console.log('onSelectedColor', event.target.value);
        if (event.target.value) {
            this.selectedColor = event.target.value;
        }
    }

    fetchColorCategoriesFromTaskIds(categoryIds: number[]): string[] {
        if (!categoryIds || !this.storedEventColors || categoryIds.length === 0 || this.storedEventColors.length === 0) {
            return [];
        }
        // console.log('Search: ', categoryIds, this.storedEventColors);
        const result: string[] = [];
        categoryIds.forEach(cId => {
            const pIdx = this.mapChildrenToParentIdCategories.findIndex(i => i[0] === cId);
            if (pIdx <= -1) {
                return;
            }
            const parentId = this.mapChildrenToParentIdCategories[pIdx][1];

            const storedIdx = this.storedEventColors.findIndex(sec => sec.object.id === parentId)
            if (storedIdx <= -1) {
                console.log('Can\'t find color for category:', parentId);
                return;
            }
            result.push(this.storedEventColors[storedIdx].value);
        });
        return result.filter((v, i, a) => a.indexOf(v) === i);
    }

    showColorCard() {
        this.isShownDisplayCard = !this.isShownDisplayCard;
    }
}
