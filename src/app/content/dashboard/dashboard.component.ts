import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../shared/models/user';
import {CategoryService} from '../shared/services/category-service';
import {Category} from '../shared/models/category';
import {EventService} from '../shared/services/event-service';
import {EventApp} from '../shared/models/event-app';
import * as moment from 'moment';
import {combineLatest} from 'rxjs';
import {ProgressDay} from '../shared/models/progress-day';
import {Subscription} from 'rxjs';
import {AuthService} from '../shared/services/auth.service';
import {ScoreService} from '../shared/services/score-service';
import {Score} from '../shared/models/score';
import PelicanUtils from '../shared/pelicanUtils';
import 'rxjs-compat/add/operator/mapTo';
import 'rxjs-compat/add/operator/mergeMap';
import {TextValue} from '../shared/models/text-value';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

    constructor(private authService: AuthService,
                private cs: CategoryService,
                private eventService: EventService,
                private scoreService: ScoreService) {
        const tmpDate = this.nowDate.clone().startOf('month');
        for (let i = 0; i < 24; i++) {
            const momObj = tmpDate.clone().subtract(i, 'month');
            const monthString = momObj.format(this.format);
            const monthName = momObj.format('yyyy MMMM');
            this.listDateMonths.push(new TextValue<string>(monthName, monthString));
            // console.log(this.nowDate.format(this.format));
        }
    }

    isLoaded = false;
    user: User;
    format = 'DD.MM.YYYY';
    events: EventApp[] = [];
    filteredEvents: EventApp[] = [];
    progresses: ProgressDay[] = [];
    categories: Category[] = [];
    allCategories: Category[] = [];
    actualCategories: Category[] = [];
    selectedProgressDay: ProgressDay;
    sub1: Subscription;
    userScore: Score;
    nowDate = moment();
    nowDateString = this.nowDate.format(this.format);
    listDateMonths: TextValue<string>[] = [];
    selectedDate = moment().startOf('month');
    selectedDateString = this.selectedDate.format(this.format);


    ngOnInit() {
        this.user = this.authService.user;
        this.refreshValues();
    }

    private refreshValues() {
        this.sub1 = combineLatest([
            this.eventService.getEvents(this.selectedDateString),
            this.cs.getCategories(),
            this.scoreService.getScore(this.user.id)]
        ).subscribe((data: [EventApp[], Category[], Score]) => {
            this.events = data[0];
            this.allCategories = data[1];
            this.userScore = data[2];
            this.categories = this.allCategories.sort((a, b) => a.name.localeCompare(b.name))
                .sort((a, b) => PelicanUtils.prettyCatName(a).localeCompare(PelicanUtils.prettyCatName(b)));
            this.actualCategories = this.categories
                .filter(e => e.deprecated === false && e.parent !== null)
                .sort((a, b) => PelicanUtils.prettyCatName(a).localeCompare(PelicanUtils.prettyCatName(b)));
            let today = this.selectedDate.clone();
            const progressDayNow = this.accumulateProcessDay(this.selectedDateString);
            this.progresses = [];
            this.progresses.push(progressDayNow);
            this.selectProgressDay(progressDayNow);
            for (let i = 1; i < moment(this.selectedDateString, this.format).daysInMonth(); i++) {
                today = today.add(1, 'd');
                const progressDay = this.accumulateProcessDay(today.format(this.format));
                this.progresses.push(progressDay);
                if (this.nowDateString === today.format(this.format)) {
                    this.selectProgressDay(progressDay);
                }
            }
            this.isLoaded = true;
        });
    }

    accumulateProcessDay(date: string): ProgressDay {
        const foundEvents = this.events.filter(e => e.date === date);
        if (foundEvents && foundEvents.length > 0) {
            let percent = 0;
            const ids: number[] = [];
            const categoryIds: number[] = [];
            // tslint:disable-next-line
            for (let i = 0; i < foundEvents.length; i++) {
                ids.push(foundEvents[i].id);
                categoryIds.push(foundEvents[i].category.id);
                const eventApp = foundEvents[i];
                percent += eventApp.balls;
            }
            if (percent > 100) {
                percent = 100;
            }
            const color = percent >= 100 ? 'green' : percent > 60 ? 'blue' : percent > 30 ? 'orange' : 'red';
            return new ProgressDay(date, ids, categoryIds, color, percent);
        } else {
            return new ProgressDay(date, [], [], 'danger', 0);
        }
    }

    selectProgressDay(day: ProgressDay) {
        this.filteredEvents = this.events.filter(e => e.date === day.date);
        this.selectedProgressDay = day;
    }

    eventWasEdited(event: EventApp) {
        // update day event content

        const idx = this.events.findIndex(e => e.id === event.id);
        if (idx > -1) {
            if (event.score === 0) {
                // если пришел 0 - значит мы должны удалить Event!
                // либо удаляем
                // получаем евент по айди и апдейтим score и удаляем сам эвент
                // this.eventService.getEventById(event.id).subscribe((prevEvent: EventApp) => {
                //   console.log('prevEvent:', prevEvent);
                // });

                this.eventService.getEventById(event.id)
                    .mergeMap((prevEvent: EventApp) => {
                        const percentOfEvent = Math.round(PelicanUtils.percentOfEvent(prevEvent, prevEvent.category));
                        return this.scoreService.operateScore(this.user.id, -percentOfEvent);
                    })
                    .mergeMap((newScore: Score) => {
                        this.userScore = newScore;
                        return this.eventService.deleteEvent(event);
                    })
                    .subscribe();


                this.events.splice(idx, 1);
            } else {
                // либо обновляем?
                // не предусмотрено.
                // нужно искать дельту между предыдущим и текущим эвентом.
                this.events[idx] = event;
            }
        } else {
            // либо добавляем
            const global = Math.round(PelicanUtils.percentOfEvent(event, event.category));
            this.events.push(event);
            this.scoreService.operateScore(this.user.id, global).subscribe((newScore: Score) => {
                this.userScore = newScore;
            });
        }
        this.filteredEvents = this.events.filter(e => e.date === this.selectedProgressDay.date);
        this.recalculateProgressDay(this.selectedProgressDay.date);
    }

    recalculateProgressDay(date: string): void {
        // recalculate day
        this.selectedProgressDay = this.accumulateProcessDay(this.selectedProgressDay.date);
        const pgIdx = this.progresses.findIndex(e => e.date === this.selectedProgressDay.date);
        this.progresses[pgIdx] = this.selectedProgressDay;
    }


    ngOnDestroy(): void {
        if (this.sub1) {
            this.sub1.unsubscribe();
        }
    }

    onSelectDateMonth(event: any) {
        console.log('event', event.target.value);
        const selectedMonth = this.listDateMonths[event.target.value];
        this.selectedDate = moment(selectedMonth.value, this.format).startOf('month');
        this.selectedDateString = this.selectedDate.format(this.format);
        this.refreshValues();
    }
}
