import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ProgressDay} from '../../shared/models/progress-day';
import {EventApp} from '../../shared/models/event-app';
import {NgForm} from '@angular/forms';
import {Category} from '../../shared/models/category';
import {EventService} from '../../shared/services/event-service';
import {CategoryService} from '../../shared/services/category-service';
import {combineLatest, Subscription} from 'rxjs';
import {AuthService} from '../../shared/services/auth.service';
import PelicanUtils from '../../shared/pelicanUtils';

@Component({
    selector: 'app-day-description',
    templateUrl: './day-description.component.html',
    styleUrls: ['./day-description.component.scss']
})
export class DayDescriptionComponent implements OnInit, OnDestroy {

    @Input() selectedProgressDay: ProgressDay;
    @Input() categories: Category[] = [];
    @Input() events: EventApp[] = [];
    public parentCategories: Category[] = [];
    public childCategories: Category[] = [];

    // message: Message;
    dropDownCategoryIdx = 0;
    scoreToAdd = 1;
    @Output() eventEdit = new EventEmitter<EventApp>();
    sub1: Subscription;
    sub2: Subscription;
    sub3: Subscription;
    sub4: Subscription;
    sub5: Subscription;

    constructor(private eventService: EventService,
                private cs: CategoryService,
                private categoryService: CategoryService,
                private authService: AuthService) {
        // this.message = new Message('success', '');
    }

    ngOnInit() {
        this.parentCategories = this.categories.filter(e => e.parent === null);
        const firstParentId = this.parentCategories[0].id;
        this.childCategories = this.categories
            .filter(e => e.parent !== null)
            .filter(e => e.parent.id === firstParentId && e.deprecated === false)
            .filter(e => e.deprecated === false);
    }

    onSubmit(form: NgForm) {
        let {score} = form.value;
        const cat: Category = this.childCategories[this.dropDownCategoryIdx];

        if (cat === null) {
            return;
        }

        if (cat.disposable) {
            if (score <= 0) {
                return;
            }

            if (score >= cat.disposableCapacity) {
                cat.deprecated = true;
                score = cat.disposableCapacity;
            }
            cat.disposableDone += score;

            const event: EventApp = new EventApp(score, cat, this.selectedProgressDay.date, this.authService.user, 0);

            this.sub2 = combineLatest([
                    this.cs.updateCategory(cat),
                    this.eventService.createEvent(event)
                ]
            ).subscribe((data: [Category, EventApp]) => {
                this.eventEdit.emit(data[1]);
            });

        } else {
            const event: EventApp = new EventApp(score, cat, this.selectedProgressDay.date, this.authService.user, 0);
            this.sub3 = this.eventService.createEvent(event).subscribe((ev: EventApp) => {
                this.eventEdit.emit(ev);
            });
        }


    }

    onCreateNewEvent() {

    }

    onParentSelect(event) {
        const parentIdx = event.target.value;
        const parent = this.parentCategories[parentIdx];
        const firstParentId = parent.id;
        this.childCategories = this.categories
            .filter(e => e.parent !== null)
            .filter(e => e.parent.id === firstParentId && e.deprecated === false)
            .filter(e => e.deprecated === false);
        // console.log('this.childCategories:', PelicanUtils.object_stringify(this.childCategories));
    }

    onChildSelect(event) {
        this.dropDownCategoryIdx = event.target.value;
    }


    isSimpleCategory(): boolean {
        const cat = this.childCategories[this.dropDownCategoryIdx];
        if (cat) {
            return cat.simple;
        }
        return true;
    }

    isSimpleCategoryById(catId: number): boolean {
        const cat = this.categories.filter(e => e.id === catId);
        if (cat && cat.length > 0) {
            return cat[0].simple;
        }
        return true;
    }


    categoryNameById(catId: number): string {
        const cat = this.categories.filter(e => e.id === catId);
        if (cat && cat.length > 0) {
            let postfix = '';
            if (cat[0].disposable) {
                postfix = ' (' + cat[0].disposableDone + ' of ' + cat[0].disposableCapacity + ')';
            }
            return cat[0].name + postfix;
        }
        return 'undefined';
    }

    updateEvents() {
        for (const ev of this.events) {
            this.sub4 = this.eventService.updateEvent(ev).subscribe((event: EventApp) => {
                this.eventEdit.emit(event);
                // this.message.text = 'Updated.';
                // window.setTimeout(() => this.message.text = '', 1000);
            });
        }
    }

    remove(event: EventApp) {
        console.log('Remove event: ', PelicanUtils.object_stringify(event))
        const idx = this.events.indexOf(event);
        if (idx > -1) {
            // this.events.splice(idx, 1);
            const category = event.category
            if (category.disposable) {
                const ctgUpdate = PelicanUtils.findCategoryById(this.childCategories, category.id)
                if (ctgUpdate) {
                    ctgUpdate.disposableDone -= event.score;
                    this.categoryService.updateCategory(ctgUpdate).subscribe(()=>{
                        event.score = 0;
                        this.eventEdit.emit(event);
                    })
                }
            }else {
                event.score = 0;
                this.eventEdit.emit(event);
            }
        }
    }

    ngOnDestroy(): void {
        if (this.sub1) {
            this.sub1.unsubscribe();
        }
        if (this.sub2) {
            this.sub2.unsubscribe();
        }

        if (this.sub3) {
            this.sub3.unsubscribe();
        }

        if (this.sub4) {
            this.sub4.unsubscribe();
        }

        if (this.sub5) {
            this.sub5.unsubscribe();
        }
    }

    prettyEventAppConvert(event: EventApp): string {
        return PelicanUtils.prettyCatName(event.category);
    }

    fastInputBalls(num: number) {
        const cat = this.childCategories[this.dropDownCategoryIdx];
        if (cat.disposable) {
            this.scoreToAdd = cat.disposableDone + num;
        } else {
            this.scoreToAdd = num;
        }
    }
}
