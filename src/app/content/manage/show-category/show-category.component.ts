import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../../shared/models/category';
import PelicanUtils from '../../shared/pelicanUtils';
import {Plan} from '../../shared/models/plan';

@Component({
    selector: 'app-show-category',
    templateUrl: './show-category.component.html',
    styleUrls: ['./show-category.component.scss']
})
export class ShowCategoryComponent implements OnInit {

    sCategory: Category;
    allCategories: Category[] = [];

    @Input()
    set selectedCategory(newValue: Category) {
        console.log('list-selected: ', newValue);
        this.sCategory = newValue;
    }

    @Input()
    set categories(newValue: Category[]) {
        console.log('list-all: ', newValue);
        this.allCategories = newValue;
        this.parentCategories = this.allCategories.filter(e => e.parent === null);
        this.parentCategories.forEach(e => e.id = +e.id);
        if (this.sCategory.parent === null) {
            this.selectedParent = +this.sCategory.id;
            console.log('parent is null!', this.selectedParent);
            this.refreshChilds(+this.selectedParent);
        } else {
            this.selectedParent = +this.sCategory.parent.id;
            const idx = this.parentCategories.findIndex(c=> c.id === this.selectedParent)
            console.log('parent not null!', this.selectedParent,idx, this.parentCategories[idx]);

            // setTimeout(() => {
            //     this.selectedParent = +this.sCategory.parent.id;
            //     const idx = this.parentCategories.findIndex(c=> c.id === this.selectedParent)
            //     console.log('parent not null!', this.selectedParent,idx, this.parentCategories[idx]);
            // }, 1000);
            this.refreshChilds(+this.sCategory.parent.id);
        }

    }

    @Output() categorySelect = new EventEmitter<Category>();
    public selectedParent = 0;
    public parentCategories: Category[] = [];
    public childCategories: Category[] = [];

    constructor() {
    }

    ngOnInit() {

    }

    prettyCatName(cat: Category): string {
        return PelicanUtils.prettyCatName(cat);
    }

    selectCategory(cat: Category) {
        this.categorySelect.emit(cat);
    }

    isNotDeprecated(cat: Category) {
        return !cat.deprecated;
    }

    isDeprecated(cat: Category) {
        return cat.deprecated;
    }

    onChangeParentCategoryModel(parendId: number) {
        const pIdx = this.parentCategories.findIndex(c => c.id === parendId);
        const parent = this.parentCategories[pIdx];
        this.sCategory = parent;
        // console.log('select parent: ', parent);
        this.refreshChilds(parent.id);
        this.categorySelect.emit(parent);
    }

    private refreshChilds(cat: number) {
        this.childCategories = this.allCategories
            .filter(e => e.parent !== null)
            .filter(e => e.parent.id === cat);
        console.log('refresh Childs', cat, this.childCategories);
    }
}
