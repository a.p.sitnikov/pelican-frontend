import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Category} from '../../shared/models/category';
import {NgForm} from '@angular/forms';
import {CategoryService} from '../../shared/services/category-service';
import {Message} from '../../shared/models/message';
import {Subscription} from 'rxjs';
import {AuthService} from '../../shared/services/auth.service';

@Component({
    selector: 'app-manage-category',
    templateUrl: './manage-category.component.html',
    styleUrls: ['./manage-category.component.scss']
})
export class ManageCategoryComponent implements OnInit, OnDestroy {

    sCategory: Category;

    @Input()
    set selectedCategory(newValue: Category) {
        console.log('Edit-select: ', newValue);
        this.sCategory = newValue;
        // console.log('sCategory', this.sCategory);
        // console.log('dropDownParentCategoryId', this.dropDownParentCategoryId);
        this.dropDownParentCategoryId = this.sCategory.parent === null ? '0' : '' + this.sCategory.parent.id;
        // console.log('set parent 1: ', this.dropDownParentCategoryId);
    }

    allCategories: Category[] = [];

    @Input()
    set categories(newValue: Category[]) {
        console.log('Edit-all: ', newValue);
        this.allCategories = newValue;
        this.parentCategories = [];
        const emptyCategory = new Category(null, 'NO PARENT', false, 0, false, 0, 0, false,
            null, false, 0);
        this.parentCategories.push(emptyCategory);
        this.allCategories.filter(c => c.parent === null).forEach(c => this.parentCategories.push(c));
        this.dropDownParentCategoryId = this.sCategory.parent === null ? '0' : this.sCategory.parent.id;
        // console.log('set parent 2: ', this.dropDownParentCategoryId);
        // console.log('allCategories:', this.allCategories);
        // console.log('parentCategories:', this.parentCategories);
    }

    parentCategories: Category[] = [];
    dropDownParentCategoryId: any = '0';
    @Output() categoryEdit = new EventEmitter<Category>();
    message: Message;
    sub1: Subscription;
    sub2: Subscription;

    constructor(private cs: CategoryService,
                private authService: AuthService) {
    }

    ngOnInit() {
        this.dropDownParentCategoryId = '0';
        this.message = new Message('success', '');
    }

    onSubmit(form: NgForm, isNew: boolean) {
        // console.log('this.createNew', isNew, this.sCategory);
        const {name, score, disposableCapacity, simple, disposable, disposableDone} = form.value;
        let parentId = 0;
        if (this.dropDownParentCategoryId !== '0') {
            parentId = +this.dropDownParentCategoryId;
        }
        let parentCategory = this.allCategories.find(e => e.id === parentId);
        // console.log('Found parent: ', parentId, this.dropDownParentCategoryId, this.allCategories.length, parentCategory);
        let cap = 0;
        let capDone = 0;
        if (this.sCategory.disposable) {
            cap = disposableCapacity;
            capDone = disposableDone;
        }
        if (parentId === 0) {
            parentCategory = null;
        }
        const ctg = new Category(parentCategory,
            name,
            simple,
            score,
            disposable,
            cap,
            capDone,
            this.sCategory.deprecated,
            this.authService.user,
            false,
            this.sCategory.id);
        if (isNew) {
            console.log('this.createNew!!!', isNew, ctg);
            ctg.id = undefined;
            this.sub1 = this.cs.createCategory(ctg)
                .subscribe((cat: Category) => {
                    this.categoryEdit.emit(cat);
                    // this.message.text = 'Edited.';
                    window.setTimeout(() => this.message.text = '', 1000);
                });
        } else {
            const children = this.allCategories.filter(e => e.parent !== null && e.parent.id === ctg.id);
            if (children.length > 0) {
                ctg.parent = null;
            }
            console.log('Update sCategory: ', isNew, ctg);
            this.sub2 = this.cs.updateCategory(ctg)
                .subscribe((cat: Category) => {
                    this.categoryEdit.emit(cat);
                    this.message.text = 'Edited.';
                    window.setTimeout(() => this.message.text = '', 1000);
                });
        }

    }

    onCategoryChange() {
        // console.log('this.dropDownCategoryId', this.dropDownCategoryId);
    }

    ngOnDestroy(): void {
        if (this.sub1) {
            this.sub1.unsubscribe();
        }

        if (this.sub2) {
            this.sub2.unsubscribe();
        }
    }
}
