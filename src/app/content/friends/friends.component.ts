import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {combineLatest} from 'rxjs';
import {ReportService} from '../shared/services/report-service';
import {UserCategoryReport} from '../shared/models/user-category-report';

@Component({
    selector: 'app-friends',
    templateUrl: './friends.component.html',
    styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit, OnDestroy {

    sub1: Subscription;
    isLoaded = false;
    usersCategoryReport: UserCategoryReport[] = [];

    constructor(
        private reportService: ReportService
    ) {
    }

    ngOnInit() {
        this.sub1 = combineLatest([
                this.reportService.allUsersReport(12),
            ]
        )
            .subscribe((data: [UserCategoryReport[]]) => {
                    this.usersCategoryReport = data[0];

                    this.isLoaded = true;
                }
            )
        ;
    }

    ngOnDestroy(): void {
        if (this.sub1) {
            this.sub1.unsubscribe();
        }
    }

}
