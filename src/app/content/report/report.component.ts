import {Component, OnDestroy, OnInit} from '@angular/core';
import {CategoryService} from '../shared/services/category-service';
import {EventService} from '../shared/services/event-service';
import {UserService} from '../shared/services/user-service';
import {EventApp} from '../shared/models/event-app';
import {Category} from '../shared/models/category';
import {User} from '../shared/models/user';
import {combineLatest} from 'rxjs';
import {ProgressDay} from '../shared/models/progress-day';
import {Subscription} from 'rxjs';
import {AuthService} from '../shared/services/auth.service';
import {CalculateProcessComponent} from './calculate.process.component';
import {BadEvent} from '../shared/models/BadEvent';
import {BadEventService} from '../shared/services/bad-event-service';
import {ReportService} from '../shared/services/report-service';
import {CommonReport} from '../shared/models/common-report';
import {LevelReport} from '../shared/models/level-report';
import {AnnualReport} from '../shared/models/annual-report';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit, OnDestroy {

    isLoaded = false;
    user: User;
    events: EventApp[] = [];
    categories: Category[] = [];
    badEvents: BadEvent[] = [];
    format = 'DD.MM.YYYY';

    common7 = [];
    common30 = [];
    common90 = [];
    commonAll7 = [];
    commonAll30 = [];
    commonAll90 = [];
    rawCategoryMonth = [];
    rawCategoryHalfYear = [];
    rawCategoryYear = [];
    entertainmentCategory = [];
    levels: LevelReport[] = [];
    annualReports: AnnualReport[] = [];
    sub1: Subscription;

    constructor(private us: UserService,
                private cs: CategoryService,
                private authService: AuthService,
                private eventService: EventService,
                private badEventService: BadEventService,
                private reportService: ReportService
    ) {
    }

    ngOnInit() {

        this.sub1 = combineLatest([
                this.reportService.commonReport(1),
                this.reportService.commonReport(6),
                this.reportService.commonReport(12),
                this.reportService.commonReportAllCategory(1),
                this.reportService.commonReportAllCategory(6),
                this.reportService.commonReportAllCategory(12),
                this.reportService.commonCategoryReport(1),
                this.reportService.commonCategoryReport(6),
                this.reportService.commonCategoryReport(12),
                this.reportService.entertainmentCategoryReport(1),
                this.reportService.levelsReport(),
                this.reportService.annualReport()
            ]
        ).subscribe((data: [
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            CommonReport[],
            LevelReport[],
            AnnualReport[]
        ]) => {
            this.common7 = data[0];
            this.common30 = data[1];
            this.common90 = data[2];
            this.commonAll7 = data[3];
            this.commonAll30 = data[4];
            this.commonAll90 = data[5];
            this.rawCategoryMonth = data[6];
            this.rawCategoryHalfYear = data[7];
            this.rawCategoryYear = data[8];
            this.entertainmentCategory = data[9];
            this.levels = data[10];
            this.annualReports = data[11];
            this.isLoaded = true;
            console.log('commonCategory7:', this.rawCategoryMonth);
            console.log('AR:', this.annualReports);
        });


    }

    calculateProcessDay(date: string): ProgressDay {
        const foundEvents = this.events.filter(e => e.date === date);
        if (foundEvents && foundEvents.length > 0) {
            let percent = 0;
            const ids: number[] = [];
            const categoryIds: number[] = [];
            // tslint:disable-next-line
            for (let i = 0; i < foundEvents.length; i++) {
                ids.push(foundEvents[i].id);
                categoryIds.push(foundEvents[i].category.id);
                const eventApp = foundEvents[i];
                const cat = this.categories.filter(c => c.id === eventApp.category.id)[0];
                if (cat && cat.simple) {
                    percent += cat.score;
                    continue;
                }
                if (eventApp.score > 0) {
                    percent += eventApp.score * 100 / cat.score;
                }
            }
            if (percent > 100) {
                percent = 100;
            }
            const color = percent >= 100 ? 'success' : percent > 60 ? 'info' : percent > 30 ? 'warning' : 'danger';
            return new ProgressDay(date, ids,categoryIds, color, percent);
        } else {
            return new ProgressDay(date, [], [],'danger', 0);
        }
    }

    getCatColorClass(cat: Category): string {
        const percent = this.getCatPercent(cat);
        return percent < 60 ? 'danger' : percent >= 100 ? 'success' : 'warning';
    }

    getColorClass(value: number): string {
        return value < 40 ? 'danger' : value >= 100 ? 'success' : 'warning';
    }

    getCatPercent(cat: Category): number {
        const percent = (100 * cat.disposableDone / cat.disposableCapacity);
        return percent > 100 ? 100 : percent;
    }

    ngOnDestroy(): void {
        if (this.sub1) {
            this.sub1.unsubscribe();
        }
    }
}
