import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Plan} from '../../shared/models/plan';

@Component({
  selector: 'app-show-plan',
  templateUrl: './show-plan.component.html',
  styleUrls: ['./show-plan.component.scss']
})
export class ShowPlanComponent implements OnInit {

  @Input() selectedPlan: Plan;
  @Output() planSelect = new EventEmitter<Plan>();
  @Input() positivePlans: Plan[] = [];
  @Input() negativePlans: Plan[] = [];
  @Input() plans: Plan[] = [];

  constructor() {
  }

  ngOnInit() {

  }

  selectPlan(plan: Plan) {
    // console.log('click select Plan: ', plan);
    this.selectedPlan = plan;
    this.planSelect.emit(plan);
  }

  filterGrandPlansAndFinished(plan: Plan) {
    return plan.isGrand && plan.isFinished;
  }

  filterNotFinished(plan: Plan) {
    return !plan.isFinished ;
  }

}
