import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {User} from '../shared/models/user';
import {AuthService} from '../shared/services/auth.service';
import {Plan} from '../shared/models/plan';
import {PlanService} from '../shared/services/plan-service';
import {ScoreService} from '../shared/services/score-service';
import {Score} from '../shared/models/score';
import {combineLatest} from 'rxjs';
import PelicanUtils from '../shared/pelicanUtils';

@Component({
    selector: 'app-bad-component',
    templateUrl: './plan.component.html',
    styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit, OnDestroy {

    plans: Plan[] = [];
    isLoaded = false;
    selectedPlan: Plan;
    score: Score;
    sub1: Subscription;
    user: User;
    positivePlans: Plan[] = [];
    negativePlans: Plan[] = [];

    constructor(private planService: PlanService,
                private authService: AuthService,
                private scoreService: ScoreService) {
    }


    ngOnInit() {
        this.sub1 = combineLatest([
                this.planService.getPlans(this.authService.user.id),
                this.scoreService.getScore(this.authService.user.id)
            ]
        ).subscribe((data: [Plan[], Score]) => {
            this.score = data[1];
            this.plans = PelicanUtils.sortPlans(data[0]);
            this.positivePlans = PelicanUtils.sortPlans(this.plans.filter(e => e.score >= 0));
            this.negativePlans = PelicanUtils.sortPlans(this.plans.filter(e => e.score < 0));
            this.positivePlans.forEach(e => {
                console.log('=> ', e.orderNumber);
            });
            this.isLoaded = true;
            this.selectedPlan = this.plans[0];
        });
    }

    selectPlan(plan: Plan) {
        this.selectedPlan = plan;
    }

    planWasEdited(plan: Plan) {
        const idx = this.plans.findIndex(e => e.id === plan.id);
        // if Plan is updating
        if (idx >= 0) {
            this.plans[idx] = plan;
            if (plan.isFinished) {
                this.scoreService.operateScore(this.authService.user.id, plan.score).subscribe((score: Score) => {
                    this.score = score;
                });
            }

            this.planService.getPlans(this.authService.user.id).subscribe(e => {
                    this.plans = PelicanUtils.sortPlans(e);
                    this.positivePlans = PelicanUtils.sortPlans(this.plans.filter(plan1 => plan1.score >= 0));
                    this.negativePlans = PelicanUtils.sortPlans(this.plans.filter(plan1 => plan1.score < 0));
                }
            );

        } else {

            // if created new plan
            this.plans.push(plan);
            this.planService.getPlans(this.authService.user.id).subscribe(e => {
                    this.plans = PelicanUtils.sortPlans(e);
                    this.positivePlans = PelicanUtils.sortPlans(this.plans.filter(plan1 => plan1.score >= 0));
                    this.negativePlans = PelicanUtils.sortPlans(this.plans.filter(plan1 => plan1.score < 0));
                }
            );
        }
        this.plans = PelicanUtils.sortPlans(this.plans);
    }


    ngOnDestroy(): void {
        if (this.sub1) {
            this.sub1.unsubscribe();
        }
    }
}
