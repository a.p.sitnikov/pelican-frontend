import {User} from '../models/user';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {EventApp} from '../models/event-app';
import {Injectable} from '@angular/core';
import {BaseApi} from './base-api';

@Injectable()
export class EventService extends BaseApi {
    constructor(public http: HttpClient) {
        super(http);
    }

    addEvent(event: EventApp): Observable<EventApp> {
        return this.post('events', event);
    }

    getEvents(date: string): Observable<EventApp[]> {
        const params = new HttpParams()
            .set('date', date);
        return this.getArray('events', params);
    }

    getAllEvents(): Observable<EventApp[]> {
      return this.getArray('events', null);
    }

    getEventById(id: number): Observable<EventApp> {
        return this.get(`events/${id}`);
    }

    updateEvent(event: EventApp): Observable<EventApp> {
        return this.put('events', event);
    }

    deleteEvent(event: EventApp): Observable<EventApp> {
        return this.delete(`events/${event.id}`);
    }

    createEvent(event: EventApp): Observable<EventApp> {
        return this.post('events', event);
    }
}
