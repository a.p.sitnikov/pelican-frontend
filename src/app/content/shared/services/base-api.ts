import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {environment} from '../../../../environments/environment';

@Injectable()
export class BaseApi {

    // public baseUrl = 'http://usque.ru:8080/pelican';
    // public baseUrl = 'http://localhost:8080/pelican';
    public baseUrl = environment.backendUrl;
    public readonly KEY_CURRENT_USER: string = 'user';

    constructor(public http: HttpClient) {

    }

    private getUrl(url: string = ''): string {
        return this.baseUrl + '/' + url;
    }

    public getAll(url: string = '', params: HttpParams): Observable<any> {
        const user: User = JSON.parse(localStorage.getItem(this.KEY_CURRENT_USER));
        const headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(user.login + ':' + user.password));
        return this.http.get<any>(this.getUrl(url), {headers: headers, params: params});
    }

    public getArray(url: string = '', params: HttpParams): Observable<any> {
        return this.getAll(url, params);
    }

    public get(url: string = ''): Observable<any> {
        const all = this.getAll(url, null);
        return all[0] ? all[0] : all;
    }

    public post(url: string = '', data: any = {}): Observable<any> {
        const user: User = JSON.parse(localStorage.getItem(this.KEY_CURRENT_USER));
        const headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(user.login + ':' + user.password));
        const some = this.http.post<any>(this.getUrl(url), data, {headers: headers});
        return some[0] ? some[0] : some;
    }

    public put(url: string = '', data: any = {}): Observable<any> {
        const user: User = JSON.parse(localStorage.getItem(this.KEY_CURRENT_USER));
        const headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(user.login + ':' + user.password));
        const some = this.http.put(this.getUrl(url), data, {headers: headers});
        return some[0] ? some[0] : some;
    }

    public delete(url: string = ''): Observable<any> {
        const user: User = JSON.parse(localStorage.getItem(this.KEY_CURRENT_USER));
        const headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(user.login + ':' + user.password));
        return this.http.delete(this.getUrl(url), {headers: headers});
    }
}
