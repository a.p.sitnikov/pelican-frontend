import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user';
import {BaseApi} from './base-api';

@Injectable()
export class UserService extends BaseApi {

    constructor(public http: HttpClient) {
        super(http);
    }

    addUser(event: User): Observable<User> {
        return this.post('users', event);
    }

    getUsers(): Observable<User[]> {
        return this.getArray('users', null);
    }

    getUserById(id: string): Observable<User> {
        return this.get(`users/${id}`);
    }

    getUserByLogin(login: string, password: string): Observable<User> {
      const url = `${this.baseUrl}/users/auth`;
      const headers = new HttpHeaders().set('Authorization', 'Basic ' + btoa(login + ':' + password));
      return this.http.get<User>(url, {headers});
      // return this.get(url);
    }

    register(user: User): Observable<User> {
        const url = `${this.baseUrl}/users/register`;
        return this.http.post<User>(url, user);
    }

}
