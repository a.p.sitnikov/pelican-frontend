import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BaseApi} from './base-api';
import {CommonReport} from '../models/common-report';
import {LevelReport} from '../models/level-report';
import {UserCategoryReport} from '../models/user-category-report';
import {AnnualReport} from '../models/annual-report';

@Injectable()
export class ReportService extends BaseApi {
    constructor(public http: HttpClient) {
        super(http);
    }

    commonReport(month: number): Observable<CommonReport[]> {
        const params = new HttpParams()
            .set('month', String(month));
        return this.getArray('report/common', params);
    }

    commonReportAllCategory(month: number): Observable<CommonReport[]> {
        const params = new HttpParams()
            .set('month', String(month));
        return this.getArray('report/common/all-category', params);
    }

    commonCategoryReport(month: number): Observable<CommonReport[]> {
        const params = new HttpParams()
            .set('month', String(month));
        return this.getArray('report/common/raw-category', params);
    }

    entertainmentReport(month: number): Observable<CommonReport[]> {
        const params = new HttpParams()
            .set('month', String(month));
        return this.getArray('report/entertainment', params);
    }

    entertainmentCategoryReport(month: number): Observable<CommonReport[]> {
        const params = new HttpParams()
            .set('month', String(month));
        return this.getArray('report/entertainment/category', params);
    }

    levelsReport(): Observable<LevelReport[]> {
        return this.getArray('report/levels', null);
    }

    allUsersReport(month: number): Observable<UserCategoryReport[]> {
        const params = new HttpParams()
            .set('month', String(month));
        return this.getArray('report/common/all-users-category', params);
    }

    annualReport(): Observable<AnnualReport[]> {
        return this.getArray('report/annual', new HttpParams());
    }
}
