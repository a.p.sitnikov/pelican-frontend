import {Category} from './models/category';
import {EventApp} from './models/event-app';
import {BadEvent} from './models/BadEvent';
import {Plan} from './models/plan';

export default class PelicanUtils {
    static doSomething(val: string) {
        return val;
    }

    static doSomethingElse(val: string) {
        return val;
    }

    static prettyCatName(cat: Category): string {
        let prefix = '';
        if (cat.parent !== null) {
            prefix = cat.parent.name + ': ';
        }
        return prefix + cat.name;
    }

    static normalPercentOfEvent(event: EventApp, cat: Category): number {
        const numberOf = this.percentOfEvent(event, cat);
        if (numberOf > 100) {
            return 100;
        }
        return numberOf;
    }

    public static object_stringify(obj: any): string {
        return JSON.stringify(obj, null, 4);
    }

    static percentOfEvent(event: EventApp, cat: Category): number {
        if (event.score === null && event.category.simple === true) {
            return event.category.score;
        }

        let percent = 0;
        if (event.score > 0) {
            percent += event.score * 100 / cat.score;
        }
        return percent;
    }

    static percentOfBadEvent(event: BadEvent): number {
        if (event.category.score > 100) {
            return 100;
        }
        return event.category.score;
    }

    static sortPlans(plan: Plan[]): Plan[] {
        return plan.sort((n1, n2) => {
            if (n1.orderNumber > n2.orderNumber) {
                return -1;
            }

            if (n1.orderNumber < n2.orderNumber) {
                return 1;
            }

            return 0;
        });
    }

    static findCategoryById(categories: Category[], id: number): Category {
        const idx = categories.findIndex(c => c.id === id);
        if (idx > -1) {
            return categories[idx];
        } else {
            return null;
        }
    }
}
