export class ProgressDay {
  constructor(
    public date: string,
    public eventIds: number[],
    public categoryIds: number[],
    public color: string,
    public percent: number,
  ) {

  }
}
