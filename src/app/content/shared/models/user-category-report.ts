import {CommonReport} from './common-report';

export class UserCategoryReport {
    constructor(
        public userName: string,
        public reports: CommonReport[],
    ) {

    }
}
