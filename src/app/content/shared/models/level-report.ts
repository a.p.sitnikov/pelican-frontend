export class LevelReport {
    constructor(
        public categoryName: string,
        public level: number,
        public currentPercent: number,
    ) {

    }
}
