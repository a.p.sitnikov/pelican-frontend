import {CommonReport} from './common-report';
import {Plan} from './plan';

export class AnnualReport {
    constructor(
        public year: number,
        public categoriesReports: CommonReport[],
        public greatPlans: Plan[]
    ) {

    }
}
