import {CommonReport} from './common-report';
import {Plan} from './plan';

export class TextValue<T> {
    constructor(
        public text: string,
        public value: T,
    ) {

    }
}

export class ObjValue<T,V> {
    constructor(
        public object: T,
        public value: V,
    ) {

    }
}
