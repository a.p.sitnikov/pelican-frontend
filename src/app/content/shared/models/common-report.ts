export class CommonReport {
    constructor(
        public name: string,
        public value: number,
        public subReport: CommonReport[],
    ) {

    }
}
