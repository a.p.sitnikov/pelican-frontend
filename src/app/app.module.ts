import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ContentModule} from './content/content.module';
import {SharedModule} from './content/shared/shared.module';
import {LoginComponent} from './login/login.component';
import {UserService} from './content/shared/services/user-service';
import {AuthService} from './content/shared/services/auth.service';
import {AuthGuard} from './content/shared/services/auth-guard';
import {NotfoundComponent} from './content/shared/components/notfound/notfound.component';
import {RegisterComponent} from './register/register.component';
import {AppConfig} from './app.config';

export function initConfig(appConfig: AppConfig) {
    return () => appConfig.load();
}

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        NotfoundComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ContentModule,
        SharedModule
    ],
    providers: [
        AppConfig,
        {provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConfig], multi: true},
        UserService,
        AuthService,
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
